import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Posts';

  posts = [
    {
      title: 'Mon premier Post',
      content: 'Pourquoi est-ce si difficile de circuler dans le nord? Parce que les voitures n arretent pas de CALER'
    },
    {
      title: 'Mon second Post',
      content: 'Que dit une imprimante dans l eau? J ai papier'
    },
    {
      title: 'Mon troisieme Post',
      content: 'Qui vit dans les tavernes? les hommes de bieres'
    }
  ];
}
