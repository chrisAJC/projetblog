import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css']
})
export class PostListItemComponent implements OnInit {

  constructor() { }

  @Input() postTitle: string;
  @Input() postContent: string;
  private loveIts: number = 0;
  private postCreated: Date= new Date();

  onLove(){
    this.loveIts++;
    console.log(this.loveIts);
  }

  onDontLove() {
    this.loveIts--;
  }

  getColor(){
    if (this.loveIts > 0) {
      return 'green';
    }
    else if (this.loveIts < 0) {
      return 'red';
    }
  }

  ngOnInit() {
  }

}
