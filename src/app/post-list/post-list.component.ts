import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {


  constructor() { }

  posts = [
    {
      title: 'Mon premier Post',
      content: 'Pourquoi est-ce si difficile de circuler dans le nord? Parce que les voitures n arretent pas de CALER'
    },
    {
      title: 'Mon second Post',
      content: 'Que dit une imprimante dans l eau? J ai papier'
    },
    {
      title: 'Mon troisieme Post',
      content: 'Qui vit dans les tavernes? les hommes de bieres'
    }
  ];


  ngOnInit() {
  }

}
